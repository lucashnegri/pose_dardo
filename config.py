PROTO_ARQ = "pose/pose_deploy_linevec.prototxt"
PESOS_ARQ = "pose/pose_iter_440000.caffemodel"
N_PONTOS = 18
POSE_PARES = [ [1,0],[1,2],[1,5],[2,3],[3,4],[5,6],[6,7],[1,8],[8,9],[9,10],[1,11],[11,12],[12,13],[0,14],[0,15],[14,16],[15,17]]
ENT_LARGURA, ENT_ALTURA = 368, 368
LIMIAR = 0.1