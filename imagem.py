"""
Código adaptado do repositório https://github.com/spmallick/learnopencv/tree/master/OpenPose
"""

import os
import argparse
import cv2
import numpy as np
import config


def carregar_modelo():
    """Carrega o modelo já treinado do arquivo."""
    return cv2.dnn.readNetFromCaffe(config.PROTO_ARQ, config.PESOS_ARQ)


def from_bytes(fbytes):
    array = np.asarray(bytearray(fbytes), dtype=np.uint8)
    return cv2.imdecode(array, cv2.IMREAD_UNCHANGED)


def to_bytes(img):
    _, buf = cv2.imencode(".jpg", img)
    return buf.tobytes()


def processar(rede, imagem, limiar=0.1):
    """
    Processa uma imagem, retornando até 18 pontos de articulação extraídos da imagem.

    rede: Modelo carregado
    imagem: Imagem a ser processada
    limiar: Limiar utilizado para a extração dos pontos de articulação
    """
    largura, altura = imagem.shape[1], imagem.shape[0]

    entrada = cv2.dnn.blobFromImage(
        imagem,
        1.0 / 255,
        (config.ENT_LARGURA, config.ENT_ALTURA),
        (0, 0, 0),
        swapRB=False,
        crop=False,
    )

    rede.setInput(entrada)
    saida = rede.forward()
    return extrair_pontos(saida, largura, altura, limiar)


def extrair_pontos(saida, largura, altura, limiar):
    """Extrai a lista de até 18 pontos de articulação da saída do modelo (mapas de probabilidade)"""
    pontos = []

    for i in range(config.N_PONTOS):
        # confidence map of corresponding body's part.
        probMap = saida[0, i, :, :]

        # Find global maxima of the probMap.
        minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

        # Scale the point to fit on the original image
        H, W = saida.shape[2], saida.shape[3]
        x = (largura * point[0]) / W
        y = (altura * point[1]) / H

        if prob > limiar:
            pontos.append((int(x), int(y)))
        else:
            pontos.append(None)

    return pontos


def desenhar_esqueleto(imagem, pontos, copia=True):
    if copia:
        imagem = np.copy(imagem)

    for pair in config.POSE_PARES:
        partA, partB = pair[0], pair[1]

        if pontos[partA] and pontos[partB]:
            cv2.line(imagem, pontos[partA], pontos[partB], (0, 255, 0), 3)
            cv2.circle(
                imagem, pontos[partA], 8, (0, 0, 255), thickness=-1, lineType=cv2.FILLED
            )

    return imagem


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "caminho", help="Caminho para a imagem contendo a mão a ser processada"
    )
    parser.add_argument(
        "-v",
        "--verboso",
        help="Adiciona mais informações na saída",
        action="store_true",
    )
    args = parser.parse_args()

    rede = carregar_modelo()
    imagem = cv2.imread(args.caminho)
    pontos = processar(rede, imagem, config.LIMIAR)

    # imprime a localização dos pontos
    if args.verboso:
        print(pontos)

    # salva a imagem com os pontos detectados e o esqueleto desenhado
    img = desenhar_esqueleto(imagem, pontos)
    caminho_saida = "saida/" + os.path.basename(args.caminho)
    cv2.imwrite(caminho_saida, img)
