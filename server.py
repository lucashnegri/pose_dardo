from flask import Flask, render_template, request, send_file, make_response
from werkzeug.utils import secure_filename

import imagem

app = Flask(__name__)
rede = imagem.carregar_modelo()

@app.route("/")
def index():
    return render_template("main.html")


@app.route("/enviar", methods=["POST"])
def enviar():
    # recupera a imagem submetida
    arq = request.files["imagem"]
    img = imagem.from_bytes(arq.read())

    # processa a imagem
    pontos = imagem.processar(rede, img)
    imagem.desenhar_esqueleto(img, pontos, copia=False)

    # retornar a imagem
    resp = make_response(imagem.to_bytes(img))
    resp.headers["Content-Type"] = "image/png"
    return resp


# utilizado somente quando chamado via python server.py, não via gunicorn
if __name__ == "__main__":
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=True, host="0.0.0.0")
