# Leia-me

Uso dos modelos treinados pelo OpenPose para detecção de pose de atletas no lançamento de dardos.

## Dependências

- OpenCV 4
- flask
- gunicorn

## Hospedagem

Heroku: https://dardo.herokuapp.com

## Créditos

Baseado no tutorial da página [LearnOpenCV.com](https://www.LearnOpenCV.com).